#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_rightButton_clicked();

    void on_leftButton_clicked();

    void on_speedSlider_valueChanged(int value);

    void on_actionOpen_Image_triggered();

    void on_resetButton_clicked();

    void on_zoomSlider_valueChanged(int value);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
