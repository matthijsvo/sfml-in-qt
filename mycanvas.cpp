#include "mycanvas.h"

MyCanvas::MyCanvas(QWidget *parent) :
    QSFMLCanvas(parent, QPoint(), QSize(parent->width(), parent->height()), 60), _rotation(100.f), _goesRight(true) {

}

MyCanvas::MyCanvas(QWidget *parent, const QPoint &position, const QSize &size) :
    QSFMLCanvas(parent, position, size), _rotation(100.f), _goesRight(true) {

}

MyCanvas::~MyCanvas() {

}

void MyCanvas::initialize() {
    _texture.loadFromFile("./assets/p.png");
    _sprite.setTexture(_texture);
    _sprite.setOrigin(_sprite.getLocalBounds().width / 2.f, _sprite.getLocalBounds().height / 2.f);
    _sprite.setPosition(getSize().x/2.f, getSize().y/2.f);
}

void MyCanvas::update() {
    clear(sf::Color(0,128,0));

    if (!sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
       _sprite.rotate(_clock.getElapsedTime().asSeconds()*_rotation);
    }

    draw(_sprite);

    _clock.restart();
}

void MyCanvas::setImage(const std::string &image) {
    _texture.loadFromFile(image);
    _sprite.setTexture(_texture, true);
    _sprite.setOrigin(_sprite.getLocalBounds().width / 2.f, _sprite.getLocalBounds().height / 2.f);
    _sprite.setPosition(getSize().x/2.f, getSize().y/2.f);
    _sprite.setRotation(0);
}

void MyCanvas::flipRotation(bool right) {
    _goesRight = right;
    if((right && _rotation < 0) || (!right && _rotation > 0)) {
        _rotation = -_rotation;
    }
}

void MyCanvas::setRotation(int rotPercentage) {
    _rotation = ((_goesRight) ? 100.f : -100.f) * ((float)rotPercentage / 100.0);
}
