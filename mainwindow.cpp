#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <SFML/Graphics.hpp>

#include <QDebug>//TODO remove

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->speedSlider->setValue(100);
    ui->zoomSlider->setValue(100);
    ui->rightButton->click();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_rightButton_clicked()
{
    ui->sfmlCanvas->flipRotation(true);
    ui->leftButton->setDisabled(false);
    ui->rightButton->setDisabled(true);
}

void MainWindow::on_leftButton_clicked()
{
    ui->sfmlCanvas->flipRotation(false);
    ui->leftButton->setDisabled(true);
    ui->rightButton->setDisabled(false);
}

void MainWindow::on_speedSlider_valueChanged(int value)
{
    ui->sfmlCanvas->setRotation(value);
}

void MainWindow::on_actionOpen_Image_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Image"), QDir::homePath(), tr("Image Files (*.png *.jpg *.bmp)"));
    ui->sfmlCanvas->setImage(fileName.toStdString());
}

void MainWindow::on_resetButton_clicked()
{
    ui->zoomSlider->setValue(100);
}

void MainWindow::on_zoomSlider_valueChanged(int value)
{
    sf::View zoomView = ui->sfmlCanvas->getDefaultView();
    zoomView.zoom((float)(200-value)/100.f);
    ui->sfmlCanvas->setView(zoomView);
}
