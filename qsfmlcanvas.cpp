#include "qsfmlcanvas.h"

#ifdef Q_WS_X11
    #include <Qt/qx11info_x11.h>
    #include <X11/Xlib.h>
#endif

QSFMLCanvas::QSFMLCanvas(QWidget *parent, const QPoint &position, const QSize &size, unsigned int frameRate) :
    QWidget(parent),
    _initialized(false) {

    // Setup some states to allow direct rendering into the widget
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_OpaquePaintEvent);
    setAttribute(Qt::WA_NoSystemBackground);

    // Set strong focus to enable keyboard events to be received
    setFocusPolicy(Qt::StrongFocus);

    // Setup the widget geometry
    move(position);
    resize(size);

    // Setup the framerate timer
    _timer.setInterval((frameRate==0)?0:(1000/frameRate));
}

QSFMLCanvas::~QSFMLCanvas() {

}

QPaintEngine *QSFMLCanvas::paintEngine() const {
    return 0;
}

void QSFMLCanvas::showEvent(QShowEvent *) {
    if(!_initialized) {
        // Under X11, we need to flush the commands sent to the server to ensure that
        // SFML will get an updated view of the windows
        #ifdef Q_WS_X11
            XFlush(QX11Info::display());
        #endif

        // Create the SFML window with the widget handle
        sf::RenderWindow::create(QWidget::winId());

        initialize();

        // Setup the timer to trigger a refresh at specified framerate
        connect(&_timer, SIGNAL(timeout()), this, SLOT(repaint()));
        _timer.start();

        _initialized = true;
    }
}

void QSFMLCanvas::paintEvent(QPaintEvent *) {
    update();
    display();
}
