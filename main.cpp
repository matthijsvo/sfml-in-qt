#include "mainwindow.h"
#include "mycanvas.h"
#include <QFrame>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle("SFML in Qt");
    w.show();

    return a.exec();
}
