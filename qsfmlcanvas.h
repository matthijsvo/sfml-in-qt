#ifndef QSFMLCANVAS_H
#define QSFMLCANVAS_H

#include <QWidget>
#include <QTimer>
#include <SFML/Graphics.hpp>

class QSFMLCanvas : public QWidget, public sf::RenderWindow
{
    Q_OBJECT
public:
    QSFMLCanvas(QWidget* parent, const QPoint& position, const QSize& size, unsigned int frameRate = 0);

    virtual ~QSFMLCanvas();

private:
    virtual void initialize() = 0;

    virtual void update() = 0;

    virtual QPaintEngine* paintEngine() const;

    virtual void showEvent(QShowEvent*);

    virtual void paintEvent(QPaintEvent*);

private:
    QTimer _timer;
    bool   _initialized;

signals:

public slots:

};

#endif // QSFMLCANVAS_H
