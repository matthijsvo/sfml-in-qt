#-------------------------------------------------
#
# Project created by QtCreator 2014-02-28T17:58:36
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SfmlQt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qsfmlcanvas.cpp \
    mycanvas.cpp

HEADERS  += mainwindow.h \
    qsfmlcanvas.h \
    mycanvas.h

FORMS    += mainwindow.ui


LIBS += -L/usr/local/lib
CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-network -lsfml-window -lsfml-system
#CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-main-d -lsfml-network-d -lsfml-window-d -lsfml-system-d
INCLUDEPATH += /usr/local/include/SFML
DEPENDPATH += /usr/local/include/SFML
