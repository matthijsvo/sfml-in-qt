#ifndef MYCANVAS_H
#define MYCANVAS_H

#include "qsfmlcanvas.h"
#include <string>

class MyCanvas : public QSFMLCanvas
{
public:
    MyCanvas(QWidget* parent);
    MyCanvas(QWidget* parent, const QPoint& position, const QSize& size);
    virtual ~MyCanvas();
    void initialize();
    void update();
    void setImage(const std::string& image);
    void flipRotation(bool right);
    void setRotation(int rotPercentage);

private:
    sf::Texture _texture;
    sf::Sprite _sprite;
    sf::Clock _clock;
    double _rotation;
    bool _goesRight;

signals:

public slots:

};

#endif // MYCANVAS_H
